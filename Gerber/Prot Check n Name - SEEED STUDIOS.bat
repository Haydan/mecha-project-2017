@echo off
REM This batch will check that all the gerber files you need are present and rename them for you ready for zipping and sending in for fabrication.
REM List of files needed are below

Setlocal enabledelayedexpansion

set MISSING=0
call :CheckFile " - CADCAM Bottom Copper.TXT", ".GBL"
call :CheckFile " - CADCAM Bottom Silk Screen.TXT", ".GBO"
call :CheckFile " - CADCAM Bottom Solder Resist.TXT", ".GBS"
call :CheckFile " - CADCAM Drill.TXT", ".TXT"
call :CheckFile " - CADCAM Mechanical 1.TXT", ".GKO"
call :CheckFile " - CADCAM Top Copper.TXT", ".GTL"
call :CheckFile " - CADCAM Top Silk Screen.TXT", ".GTO"
call :CheckFile " - CADCAM Top Solder Resist.TXT", ".GTS"
REM if exist "Internal Plane 1.ger" call :CheckFile "Internal Plane 2.ger", ""

if %MISSING% EQU 0 (
    echo Success - all files found!
    pause
    goto :eof
) else (
    echo %MISSING% file^(s^) missing.
    pause
    goto :eof
)
REM Rename as found or flag erro

:CheckFile
set "_string=%~1"
if not exist *%1 (
    echo ERROR: Missing %1!
    set /a MISSING+=1
) else (
    for /F "delims=" %%G in ('dir /B "*%~1"') do (
        set "_fileA=%%~G"
        set "_fileB=!_fileA:%_string%=!"
        rem next `rename` command is merely ECHOed for debugging purposes
        ren "%%~G" "!_fileB!%~2"
    )
)
goto :eof